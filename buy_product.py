import psycopg2

price_request = "SELECT price FROM Shop WHERE product = %(product)s"
buy_decrease_balance = f"UPDATE Player SET balance = balance - ({price_request}) * %(amount)s WHERE username = %(username)s"
buy_decrease_stock = "UPDATE Shop SET in_stock = in_stock - %(amount)s WHERE product = %(product)s"


def get_connection():
    return psycopg2.connect(
        dbname="postgres",
        user="postgres",
        password="example",
        host="127.0.0.1",
        port=5432
    )  # TODO add your values here


def buy_product(username, product, amount):
    obj = {"product": product, "username": username, "amount": amount}
    with get_connection() as conn:
        with conn.cursor() as cur:
            try:
                # Decrement balance and stock
                cur.execute(buy_decrease_balance, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong username")

                cur.execute(buy_decrease_stock, obj)
                if cur.rowcount != 1:
                    raise Exception("Wrong product or out of stock")

                # Add purchased products to inventory
                cur.execute("SELECT amount FROM Inventory WHERE username = %(username)s AND product = %(product)s", obj)
                existing_amount = cur.fetchone()

                if existing_amount is not None:
                    total_amount = existing_amount[0] + amount
                    if total_amount > 100:
                        raise Exception("Inventory full")

                    cur.execute("UPDATE Inventory SET amount = %(total_amount)s WHERE username = %(username)s AND product = %(product)s", {"total_amount": total_amount, **obj})
                else:
                    cur.execute("INSERT INTO Inventory (username, product, amount) VALUES (%(username)s, %(product)s, %(amount)s)", obj)

                conn.commit()
            except psycopg2.errors.CheckViolation as e:
                conn.rollback()
                raise Exception("Bad balance or product is out of stock")


def create_table_if_not_exists(table_name, table_columns):
    with get_connection() as conn:
        with conn.cursor() as cur:
            cur.execute(f"""
                CREATE TABLE IF NOT EXISTS {table_name} (
                    {table_columns}
                )
            """)

create_table_if_not_exists("Inventory", "username TEXT, product TEXT, amount INT")
buy_product('Alice', 'marshmello', 1)